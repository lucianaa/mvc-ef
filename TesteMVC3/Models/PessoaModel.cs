﻿using System.ComponentModel.DataAnnotations;
public class PessoaModel
{
    [Required]
    [DataType(DataType.Currency)]
    [Display(Name = "Código")]
    public int Id { get; set; }

    [Required]
    [DataType(DataType.Text)]
    [Display(Name = "Nome")]
    public string Nome { get; set; }

    [Required]
    [DataType(DataType.Text)]
    [Display(Name = "Endereço")]
    public string Endereco { get; set; }

    [Required]
    [DataType(DataType.Date)]
    [Display(Name = "Data Nascimento")]
    public string DataNascimento { get; set; }

}