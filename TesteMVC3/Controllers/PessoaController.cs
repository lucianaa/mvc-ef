﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects;
//using System.Data.Objects;

namespace TesteMVC3.Controllers
{
    public class PessoaController : Controller
    {
        //
        // GET: /Pessoa/

        public ActionResult Index()
        {
            return View(this.LoadData());
        }

        //
        // GET: /Pessoa/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Pessoa/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Pessoa/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Pessoa/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Pessoa/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Pessoa/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Pessoa/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private List<PessoaModel> LoadData()
        {
            
            List<PessoaModel> lst = new List<PessoaModel>();
            /* entity framework*/
            //apenas para teste, mas fazer isso via repository 
            cadastroEntities contextoDados = new cadastroEntities();
            List<Cliente> clientelista;
            clientelista = new List<Cliente>();
            ObjectQuery<Cliente> clienteconsulta = contextoDados.Cliente.Where("it.nome <> 'nada'");

            clientelista = clienteconsulta.ToList();


            lst.Add(new PessoaModel() { Id = clientelista[0].codigo, Nome = clientelista[0].nome });
            lst.Add(new PessoaModel() { Id = clientelista[1].codigo, Nome = clientelista[1].nome });

            return lst;
            
            /*
            List<PessoaModel> lst = new List<PessoaModel>();
                        
            lst.Add(new PessoaModel() { Id = 1, DataNascimento = "30/01/1901", Endereco = "Rua 1", Nome = "Pessoa 1" });
            lst.Add(new PessoaModel() { Id = 2, DataNascimento = "30/02/1902", Endereco = "Rua 2", Nome = "Pessoa 2" });
            lst.Add(new PessoaModel() { Id = 3, DataNascimento = "30/03/1901", Endereco = "Rua 3", Nome = "Pessoa 3" });
            lst.Add(new PessoaModel() { Id = 4, DataNascimento = "30/04/1904", Endereco = "Rua 4", Nome = "Pessoa 4" });
            lst.Add(new PessoaModel() { Id = 5, DataNascimento = "30/05/1905", Endereco = "Rua 5", Nome = "Pessoa 5" });

            return lst; */
        }
    }
}
